import cherrypy
import re, json
from movies_library import _movie_database

class MovieController(object):

    def __init__(self, mdb=None):
        if mdb is None:
            self.mdb = _movie_database()
        else:
            self.mdb = mdb

        self.mdb.load_movies('movies.dat')

    def GET_KEY(self, movie_id):
        '''when GET request for /movies/movie_id comes in, then we respond with json string'''
        output = {'result':'success'}
        movie_id = int(movie_id)

        try:
            movie = self.mdb.get_movie(movie_id)
            if movie is not None:
                output['id'] = movie_id
                output['title'] = movie[0]
                output['genres'] = movie[1]
            else:
                output['result'] = 'error'
                output['message'] = 'movie not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def PUT_KEY(self, movie_id):
        '''when PUT request for /movies/movie_id comes in, then we change that movie in the mdb'''
        output = {'result':'success'}
        movie_id = int(movie_id)

        data = json.loads(cherrypy.request.body.read().decode('utf-8'))

        movie = list()
        movie.append(data['title'])
        movie.append(data['genres'])

        self.mdb.set_movie(movie_id, movie)

        return json.dumps(output)

    def DELETE_KEY(self, movie_id):
        '''when DELETE for /movies/movie_id comes in, we remove just that movie from mdb'''
        #TODO
        output = {"result":"success"}
        self.mdb.delete_movie(int(movie_id))
        return json.dumps(output)

    def GET_INDEX(self):
        '''when GET request for /movies/ comes in, we respond with all the movie information in a json str'''
        output = {'result':'success'}
        output['movies'] = []

        try:
            for mid in self.mdb.get_movies():
                movie = self.mdb.get_movie(mid)
                dmovie = {'id':mid, 'title':movie[0],
                                'genres':movie[1]}
                output['movies'].append(dmovie)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def POST_INDEX(self):
        '''when POST for /movies/ comes in, we take title and genres from body of request, and respond with the new movie_id and more'''
        #TODO
        output = {"result":"success"}
        new_mid = 0
        try:
            # this will fail if no movies in DB
            new_mid = max(self.mdb.get_movies()) + 1
        except Exception:
            pass

        try:
            body_obj = json.loads(cherrypy.request.body.read().decode('utf-8'))
            self.mdb.set_movie(new_mid, [body_obj['title'], body_obj['genres']])
            output['id'] = new_mid
        except ValueError:
            output = { "result": "error", "message": "invalid json body" }
        except KeyError:
            output = { "result": "error", "message": "must have title and genre values set in JSON body" }
        except Exception as ex:
            output = { "result": "error", "message": str(ex) }
        return json.dumps(output)

    def DELETE_INDEX(self):
        '''when DELETE for /movies/ comes in, we remove each existing movie from mdb object'''
        #TODO
        output = {"result":"success"}

        # make copy since we'll be modifying mdb
        for mid in [mid for mid in self.mdb.get_movies()]:
            self.mdb.delete_movie(mid)
        return json.dumps(output)
