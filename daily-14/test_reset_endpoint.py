import unittest
import requests
import json

class TestReset(unittest.TestCase):

    #SITE_URL = 'http://student05.cse.nd.edu:51090' # replace with your port id
    SITE_URL = 'http://localhost:51090' # replace with your port id
    print("Testing for server: " + SITE_URL)
    RESET_URL  = SITE_URL + '/reset/'
    MOVIES_URL = SITE_URL + '/movies/'

    def reset_data(self):
        requests.put(self.RESET_URL, data='{}')

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_put_reset_index(self):
        # setup
        self.reset_data()
        og_all_movies = requests.get(self.MOVIES_URL).content.decode('utf-8')
        existing_mid = 35

        # insert new movie
        new_movie = {"title":"New Movie", "genres":"Genre1|Genre2"}
        resp_body = requests.post(self.MOVIES_URL, data=json.dumps(new_movie)).content.decode('utf-8')
        self.assertTrue(self.is_json(resp_body))
        resp_obj = json.loads(resp_body)
        self.assertEqual('success', resp_obj['result'])
        self.assertTrue('id' in resp_obj)
        new_mid = resp_obj['id']

        # update existing movie
        update_movie = {'title':'Updated Movie', 'genres':'Genre3|Genre4'}
        resp_body = requests.put(self.MOVIES_URL+str(existing_mid), data=json.dumps(update_movie)).content.decode('utf-8')
        self.assertTrue(self.is_json(resp_body))
        resp_obj = json.loads(resp_body)
        self.assertEqual({'result':'success'}, resp_obj) 

        # reset DB
        resp_body = requests.put(self.RESET_URL, data='{}').content.decode('utf-8')
        self.assertTrue(self.is_json(resp_body))
        self.assertEqual({'result':'success'}, json.loads(resp_body))

        # check everything is the same
        new_db_state = requests.get(self.MOVIES_URL).content.decode('utf-8')
        self.assertEqual(new_db_state, og_all_movies)


    def test_put_reset_key(self):
	    #TODO write this entire test

        # setup
        self.reset_data()
        mid = 35
        og_movie_state = requests.get(self.MOVIES_URL+str(mid)).content.decode('utf-8')

        # update existing movie
        updated_movie = {'title':'Updated Movie', 'genres':'GenreX|GenreY'}
        resp_body = requests.put(self.MOVIES_URL+str(mid), data=json.dumps(updated_movie)).content.decode('utf-8')
        self.assertTrue(self.is_json(resp_body))
        resp_obj = json.loads(resp_body)
        self.assertEqual({'result':'success'}, resp_obj) 

        # reset movie
        resp_body = requests.put(self.RESET_URL+str(mid), data='{}').content.decode('utf-8')
        self.assertTrue(self.is_json(resp_body))
        self.assertEqual({'result':'success'}, json.loads(resp_body))

        # check everything is the same
        new_movie_state = requests.get(self.MOVIES_URL+str(mid)).content.decode('utf-8')
        self.assertEqual(new_movie_state, og_movie_state)
        
	
if __name__ == "__main__":
    unittest.main()

