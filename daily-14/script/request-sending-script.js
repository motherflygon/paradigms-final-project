const getFormInfo = () => {
    console.log('entered getFormInfo');
    const serverAddr = document.getElementById("select-server-address").value;
    const port = document.getElementById("input-port-number").value;
    const key = document.getElementById("input-key").value;
    const useKey = !!document.getElementById("checkbox-use-key").checked;
    const useBody = !!document.getElementById("checkbox-use-message").checked;
    const body = document.getElementById("text-message-body").value;
    console.log("serverAddr: " + serverAddr);
    console.log("port: " + port);
    console.log("key: " + key);
    console.log("useKey: " + useKey);
    console.log("useBody: " + useBody);
    console.log("body: " + body);

    const radioButtonIDList = ["radio-get", "radio-put", "radio-post", "radio-delete"];
    const radioButtonList = radioButtonIDList.map((id) => (document.getElementById(id)));
    let requestType;
    for (var i = 0; i < radioButtonList.length; i++){
        button = radioButtonList[i];
        if (button.checked){
            requestType = button.value;
        }
    }

    console.log("requestType: " + requestType);

    makeRequest(serverAddr, port, requestType, key, useKey, useBody, body);

}

const makeRequest = (serverAddr, port, requestType, key, useKey, useBody, body) => {
    console.log("entered makeRequest");
    let xhr = new XMLHttpRequest();
    const url = serverAddr + ":" + port + "/movies/" + (useKey ? key : "");
    xhr.open(requestType, url, true);

    xhr.onload = (response) => {
        document.getElementById("answer-label").innerHTML = xhr.responseText; 
    }

    xhr.onerror = () => { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    xhr.send(useBody ? body : null);
}

let submitButton = document.getElementById('send-button');
let clearButton = document.getElementById("clear-button");
submitButton.onmouseup = getFormInfo;
clearButton.onmouseup = () => {document.getElementById("text-message-body").value = ""}