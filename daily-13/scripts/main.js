console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var name = document.getElementById("name-text").value;
    console.log('Name you entered is ' + name);
    makeNetworkCallToGenderApi(name);

} // end of get form info

function makeNetworkCallToGenderApi(name){
    console.log('entered gender call' + name);
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.genderize.io/?name=" + name;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    xhr.onload = function(e) { // triggered when response is received
        console.log(xhr.responseText);
        updateGenderWithResponse(name, xhr.responseText);
    }

    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateGenderWithResponse(name, response_text){
    var response_json = JSON.parse(response_text);
    var label1 = document.getElementById("response-line1");

    if(response_json['gender'] == null){
        label1.innerHTML = 'Apologies, we could not find your name.'
    } else{
        label1.innerHTML =  name + ', your gender is ' + response_json['gender'];
        let gender = response_json['gender'];
        makeNetworkCallToUserGenerator(gender);
    }
} // end of updateAgeWithResponse

function makeNetworkCallToUserGenerator(gender){
    console.log('entered user generator call' + gender);
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://randomuser.me/api/?gender=" + gender;
    console.log(url);
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    xhr.onload = function(e) { // triggered when response is received
        console.log(xhr.responseText);
        let response_json = JSON.parse(xhr.responseText);
        updatePicture(response_json["results"][0]["picture"]["medium"]);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updatePicture(picURL){
    var label2 = document.getElementById("response-line2");
    label2.innerHTML = "Here is what you might look like:";

    // dynamically adding label
    img_item = document.createElement("img"); // "label" is a classname
    img_item.setAttribute("src", picURL);

    var response_div = document.getElementById("response-div");

    while (response_div.firstChild) {
        response_div.removeChild(response_div.firstChild);
    }
    response_div.appendChild(img_item);

} // end of updateTriviaWithResponse
