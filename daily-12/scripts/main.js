var submitButton = document.getElementById('submit-button')
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log("Entered get Form Info!")
    // get text from title, author and story
    let person_text = document.getElementById('person-text').value;
    let verb_text = document.getElementById('verb-text').value;
    let location_text = document.getElementById('location-text').value;
    let emotion_text = document.getElementById('emotion-text').value;
    console.log('person:' + person_text + ' verb: ' + verb_text + ' location ' + location_text + ' emotion ' + emotion_text);


    // get checkbox state
    let isItalics = false
    if (document.getElementById('checkbox-italics-value').checked) {
        console.log('detected italics!');
        isItalics = true
    }

    // make dictionary
    story_dict = {};
    story_dict['person'] = person_text;
    story_dict['verb'] = verb_text;
    story_dict['location'] = location_text;
    story_dict['emotion'] = emotion_text;
    story_dict["italics"] = isItalics
    console.log(story_dict);

    displayStory(story_dict);
}

function displayStory(story_dict){
    console.log("entered displayStory!");
    let storyText = story_dict["person"] + " was " + story_dict["verb"] + " in " + story_dict["location"] + ", feeling " + story_dict["emotion"];
    console.log(storyText);
    const story_display = document.getElementById('story-display');
    story_display.innerHTML = storyText;
    if (story_dict["italics"]) {
        story_display.innerHTML = storyText.italics();
    }
}
